package com.constructor;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class MainClass2 {
	public static void main(String[] args) {
		ApplicationContext context = new ClassPathXmlApplicationContext("BeansConstructorRef.xml");
		Candidate can = (Candidate) context.getBean("candidate");
		Address add = (Address) context.getBean("address");
		
		System.out.println("Name    : " + can.getName());
		System.out.println("Age     : " + can.getAge());
		System.out.println("Address : " + can.getAddrs().getDoorNo());
		System.out.println("Street  : " + add.getStreet());
		System.out.println("Area    : " + add.getArea());
	}
	
	/**
	 * The obvious change required now, is in the configuration metadata xml (Beans.xml).
	 * Instead of setting values using the <property name=" " value=" " />(thereby calling
	 * the setter method) we can now make use of <constructor-arg> to call the constructor
	 * with the appropriate parameters.
	 * */
}
