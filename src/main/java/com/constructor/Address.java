package com.constructor;

public class Address {
	private String	doorNo;
	private String	street;
	private String	area;
	
	public Address(String doorNo, String street, String area) {
		this.doorNo = doorNo;
		this.street = street;
		this.area = area;
	}
	
	public String getArea() {
		return area;
	}
	
	public void setArea(String area) {
		this.area = area;
	}
	
	public String getDoorNo() {
		return doorNo;
	}
	
	public void setDoorNo(String doorNo) {
		this.doorNo = doorNo;
	}
	
	public String getStreet() {
		return street;
	}
	
	public void setStreet(String street) {
		this.street = street;
	}
	
/**
 * In case of setter injection, we know that the property values are assigned to the
 * attributes of the class using the setter methods. In that case, default constructor
 * is used for creating the instance. But here in constructor injection , we explicitly
 * pass values to the constructor of the class from the metadata. This means that an
 * explicit constructor has to be provided in the bean classes with parameters
 * appropriate to what is about to be passed from the configuration metadata file.
 * */
	
/**
 * In case the parameters passed for the bean from the metadata file and the constructor
 * parameters in the corresponding class does not match, it results in
 * Caused by: org.springframework.beans.factory.BeanCreationException: Error creating bean
 * with name ‘address’ defined in class path resource [Beans.xml]: Could not resolve
 * matching constructor
 * */
}
