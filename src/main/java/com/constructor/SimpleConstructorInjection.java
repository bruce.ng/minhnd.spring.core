package com.constructor;

public class SimpleConstructorInjection {
	
	public SimpleConstructorInjection(String message) {
		this.message = message;
	}
	
	private String	message;
	
	public void setMessage(String message) {
		this.message = message;
	}
	
	public void getMessage() {
		System.out.println("Your Message : " + message);
	}
}
