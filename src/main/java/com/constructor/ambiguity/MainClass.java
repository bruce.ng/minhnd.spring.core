package com.constructor.ambiguity;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class MainClass {
	
	public static void main(String[] args) {
		
		ApplicationContext context = new ClassPathXmlApplicationContext("BeansConstructorAmbiguity.xml");
		InjectionAmbiguity amb = (InjectionAmbiguity) context.getBean("ambiguity");
		
		System.out.println("name " + amb.getName());
		System.out.println("Age " + amb.getAge());
		System.out.println("Score " + amb.getScore());
		
	}
}
