package com.constructor;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class MainClass1 {
	public static void main(String[] args) {
		ApplicationContext context = new ClassPathXmlApplicationContext("BeansConstructor.xml");
		SimpleConstructorInjection obj = (SimpleConstructorInjection) context.getBean("simpleConstructorInjection");
		obj.getMessage();
	}
}
