package com.collection.factory;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class MainClass {
	
	public static void main(String[] args) {
		ApplicationContext context = new ClassPathXmlApplicationContext("BeansCollectionFactory.xml");
		ClassABC abc = (ClassABC) context.getBean("abc");
		System.out.println(abc);
	}
	
}
