package com.collection.factory;

import java.util.List;
import java.util.Map;
import java.util.Set;

public class ClassABC {
	
	private List	listVariable;
	private Map		mapVariable;
	private Set		setVariable;
	
	public List getListVariable() {
		return listVariable;
	}
	
	public void setListVariable(List listVariable) {
		this.listVariable = listVariable;
	}
	
	public Map getMapVariable() {
		return mapVariable;
	}
	
	public void setMapVariable(Map mapVariable) {
		this.mapVariable = mapVariable;
	}
	
	public Set getSetVariable() {
		return setVariable;
	}
	
	public void setSetVariable(Set setVariable) {
		this.setVariable = setVariable;
	}
	
	public String toString() {
		return ("listVariable \t" + listVariable + "\nsetVariable \t" + setVariable + "\nmapVariable \t" + mapVariable);
	}
	
	/**
	 * Further let us venture into having concrete 
	 * - List collection (ArrayList and LinkedList),
	 * - Set collection(HashSet and TreeSet)
	 * - Map collection(HashMap and TreeMap)
	 * */
}
