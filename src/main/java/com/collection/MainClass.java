package com.collection;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class MainClass {
	
	public static void main(String[] args) {
		ApplicationContext context = new ClassPathXmlApplicationContext("BeansCollection.xml");
		CollectionBean bean = (CollectionBean) context.getBean("testBean");
		Amount amt = (Amount) context.getBean("amount");
		System.out.println("List of String:t" + bean.getStringListVariable());
		System.out.println("List          :t" + bean.getListVariable());
		System.out.println("set           :t" + bean.getSetVariable());
		System.out.println("Map           :t" + bean.getMapVariable());
		System.out.println("Properties    :t" + bean.getPropVariable());
//		Object sou;
//	    sou
	}
}
