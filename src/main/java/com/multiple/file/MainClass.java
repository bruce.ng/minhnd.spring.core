package com.multiple.file;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.multiple.file.spEL.Candidate;

public class MainClass {
	
	public static void main(String[] args) {
		
		ApplicationContext context = new ClassPathXmlApplicationContext(new String[] { "multi/Beans.xml", "multi/elBeans.xml" });
		Candidate can = (Candidate) context.getBean("candidate");
		System.out.println(can);
	}
}