package com.multiple.file.spEL;

import com.multiple.file.Amount;

public class Candidate {
	
	private String	name;
	private int			age;
	private Address	addrs;
	private String	area;
	private Amount	amt;
	
	public Amount getAmt() {
		return amt;
	}
	
	public void setAmt(Amount amt) {
		this.amt = amt;
	}
	
	public String getArea() {
		return area;
	}
	
	public void setArea(String area) {
		this.area = area;
	}
	
	public Candidate() {
		this.name = null;
		this.age = 0;
		this.addrs = null;
	}
	
	public Candidate(String name, int age, Address addrs) {
		this.name = name;
		this.age = age;
		this.addrs = addrs;
	}
	
	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public int getAge() {
		return age;
	}
	
	public void setAge(int age) {
		this.age = age;
	}
	
	public Address getAddrs() {
		return addrs;
	}
	
	public void setAddrs(Address addrs) {
		this.addrs = addrs;
	}
	
	public String toString() {
		return ("name:" + name + "\nage:" + age + "\naddrs:" + addrs + "\namt:" + amt + "\n");
		
	}
}
