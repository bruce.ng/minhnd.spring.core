package com.scope.singleton;

public class Amount {
	
	private String	val;
	
	public String getVal() {
		return val;
	}
	
	public void setVal(String val) {
		this.val = val;
	}
	
	/**
	 * In singleton scope, only a single bean instance is returned per Spring IoC container
	 * */
}
