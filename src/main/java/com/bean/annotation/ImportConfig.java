package com.bean.annotation;

import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;


@Configuration
@Import({ CustomerConfig.class, SchedulerConfig.class })
public class ImportConfig {
	
}
