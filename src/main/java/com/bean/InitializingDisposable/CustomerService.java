
package com.bean.InitializingDisposable;

import org.springframework.beans.factory.DisposableBean;
import org.springframework.beans.factory.InitializingBean;

public class CustomerService implements InitializingBean, DisposableBean {
	
	private String	message;
	
	@Override
	public void afterPropertiesSet() throws Exception {
		System.out.println("Init method after properties are set : " + message);
	}
	
	@Override
	public void destroy() throws Exception {
		System.out.println("Spring Container is destroy! Customer clean up");
	}
	
	public String getMessage() {
		return message;
	}
	
	public void setMessage(String message) {
		this.message = message;
	}
}
