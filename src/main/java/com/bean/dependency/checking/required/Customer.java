
package com.bean.dependency.checking.required;

import org.springframework.beans.factory.annotation.Required;

public class Customer {
	
	private Person	person;
	private int			type;
	private String	action;
	
	public Person getPerson() {
		return person;
	}
	
	@Required
	public void setPerson(Person person) {
		this.person = person;
	}
	
	/**
	 * Spring’s dependency checking in bean configuration file is used to make sure all properties of a certain types (primitive, collection or object) have been set. In most scenarios, you just need to make sure a particular property has been set, but not all properties..
	 * For this case, you need @Required annotation
	 * */
	
}
