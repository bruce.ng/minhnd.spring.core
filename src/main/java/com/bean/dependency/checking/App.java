package com.bean.dependency.checking;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;


public class App {
	
	public static void main(String[] args) {
		
		ApplicationContext context = new ClassPathXmlApplicationContext(new String[] { "beans/BeansDependencyChecking.xml" });
		Customer cus = (Customer) context.getBean("CustomerBean");
		System.out.println("printing Customer in main class " + cus);
	}
}
