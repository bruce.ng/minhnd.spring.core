package com.bean.lifecycle;

import org.springframework.context.support.ClassPathXmlApplicationContext;

public class MainClass {
	
	public static void main(String[] args) {
		
		//ApplicationContext context = new ClassPathXmlApplicationContext(new String[] { "BeansLifecycle.xml" });
		//Amount amt = (Amount) context.getBean("amount");
		//System.out.println("printing amount in main class " + amt);
		
		/**
		 * Notice that, the destroy method has never been called. This is because the context is not closed.
		 * 
		 * But, close() method is not defined on ApplicationContext.
		 * The method is rather defined on the implementation ClassPathXmlApplicationContext.
		 * So let us modify that part as well  to implement our changes
		 * 
		 * 
		 * */
		ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext(new String[] { "BeansLifecycle.xml" });
		context.close();

		//Amount amt = (Amount) context.getBean("amount");
		//System.out.println("printing amount in main class " + amt);
		//context.close(); // destroy method is called here
	}
}
