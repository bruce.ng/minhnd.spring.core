
package com.autowiring.byQualifier;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

import com.autowiring.byQualifier.Person;

public class Customer {
	
	@Autowired
	@Qualifier("personA")
	private Person person;
	
	//...
	
	public Person getPerson() {
		return person;
	}
	
	public void setPerson(Person person) {
		this.person = person;
	}
	
}
