package com.simpleCodeStuffs;

public class HelloWorld {
	
	public HelloWorld() {
		System.out.println("In constructor of HelloWorld");
	}
	
	private String	message;
	
	public void setMessage(String message) {
		this.message = message;
	}
	
	public void getMessage() {
		System.out.println("Your Message : " + message);
	}
	
}