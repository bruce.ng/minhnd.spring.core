package com.simpleCodeStuffs;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class JavaConfig {
	
	@Bean(name = "helloWorld")
	public HelloWorld helloWorld() {
		HelloWorld bean = new HelloWorld();
		System.out.println("helloWorld bean through Java Config");
		bean.setMessage("JavaConfig msg- SimpleCodeStuffs");
		return bean;
	}
	
	@Bean
	public Candidate candidate() {
		return new Candidate(address());
	}
	
	@Bean
	public Address address() {
		return new Address();
	}
}
