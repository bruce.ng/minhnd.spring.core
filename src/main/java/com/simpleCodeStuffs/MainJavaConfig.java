package com.simpleCodeStuffs;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class MainJavaConfig {
	
	public static void main(String args[]) {
		
		ApplicationContext context = new AnnotationConfigApplicationContext(JavaConfig.class);
		
		HelloWorld obj = (HelloWorld) context.getBean("helloWorld");
		
		obj.getMessage();
		
	}
}
