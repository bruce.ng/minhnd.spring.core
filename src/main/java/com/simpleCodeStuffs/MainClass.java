package com.simpleCodeStuffs;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class MainClass {
	public static void main(String[] args) {
		ApplicationContext context = new ClassPathXmlApplicationContext("Beans.xml");
		
		HelloWorld obj = (HelloWorld) context.getBean("helloWorld");
		obj.getMessage();
	}
}

/**
		1. Application context is created using framework API ClassPathXmlApplicationContext().
This API loads beans configuration file beans.xml (in our case) which contains details of
the beans and the corresponding property values. Then it takes care of creating and
initializing all the objects i.e., beans mentioned in the configuration file.

2. The configuration xml file has been loaded. Now, the bean ID is used to obtain a generic
object of the required class using getBean() method on the created context. This method
returns a generic object which can be finally casted to actual object. Once this object is
at hand, accessing the methods of this class is just the usual process.
*/
