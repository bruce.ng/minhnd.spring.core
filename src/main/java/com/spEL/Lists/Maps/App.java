package com.spEL.Lists.Maps;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class App {

	public static void main(String[] args) {
		//ApplicationContext context = new ClassPathXmlApplicationContext("spEL/elBeansDotOperator.xml");
		
		ApplicationContext context2 = new AnnotationConfigApplicationContext();
		Customer obj = (Customer) context2.getBean("customerBean");

		System.out.println(obj);
	}
}
