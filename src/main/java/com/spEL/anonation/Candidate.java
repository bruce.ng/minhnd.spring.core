package com.spEL.anonation;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component("elCandidate")
public class Candidate {
	
	@Value("Poorni")
	private String	name;
	
	@Value("27")
	private int			age;
	
	@Value("#{elAddress}")
	private Address	addrs;
	
	@Value("#{elAddress.area}")
	private String	area;
	
	public String getArea() {
		return area;
	}
	
	public void setArea(String area) {
		this.area = area;
	}
	
	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public int getAge() {
		return age;
	}
	
	public void setAge(int age) {
		this.age = age;
	}
	
	public Address getAddrs() {
		return addrs;
	}
	
	public void setAddrs(Address addrs) {
		this.addrs = addrs;
	}
}
