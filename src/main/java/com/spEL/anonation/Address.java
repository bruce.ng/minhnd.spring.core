package com.spEL.anonation;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component("elAddress")
public class Address {
	
	@Value("22/7")
	private String	doorNo;
	
	@Value("MKM Street")
	private String	street;
	
	@Value("Ambattur")
	private String	area;
	
	public String getArea() {
		return area;
	}
	
	public void setArea(String area) {
		this.area = area;
	}
	
	public String getDoorNo() {
		return doorNo;
	}
	
	public void setDoorNo(String doorNo) {
		this.doorNo = doorNo;
	}
	
	public String getStreet() {
		return street;
	}
	
	public void setStreet(String street) {
		this.street = street;
	}
}
