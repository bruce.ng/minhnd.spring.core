package com.spEL.accessing;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Component;

//The bean is defined using @Component annotation.
//The values of the Map and List are set in the constructor.

@Component("testBean")
public class Bean {
	
	private Map<String, String>	name;
	private List<Integer>				price;
	
	public Bean() {
		name = new HashMap<String, String>();
		name.put("a", "Sandy");
		name.put("b", "Poorni");
		name.put("c", "Gokul");
		price = new ArrayList<Integer>();
		price.add(150);
		price.add(200);
		price.add(570);
	}
	
	public Map<String, String> getName() {
		return name;
	}
	
	public void setName(Map<String, String> name) {
		this.name = name;
	}
	
	public List<Integer> getPrice() {
		return price;
	}
	
	public void setPrice(List<Integer> price) {
		this.price = price;
	}
}
