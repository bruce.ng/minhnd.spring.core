package com.spEL.accessing;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

//In case of MAP {#beanID.<propertyName>[MapIndex]}
//In case of List {#beanID.<propertyName>[indexValue]}

@Component("custBean")
public class Customer {
	
	@Value("#{testBean.name['c']}")
	private String	name;
	
	@Value("#{testBean.price[0]}")
	private double	bill;
	
	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public double getBill() {
		return bill;
	}
	
	public void setBill(double bill) {
		this.bill = bill;
	}
}
