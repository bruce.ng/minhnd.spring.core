package com.auto.scanning.components;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class App {
	public static void main(String[] args) {
		ApplicationContext context = new ClassPathXmlApplicationContext(
				new String[] { "AutoScanningComponents/Spring-Customer.xml" });

		//CustomerService cust = (CustomerService) context.getBean("customerService");
		CustomerService cust = (CustomerService) context.getBean("minhnd");
		System.out.println(cust);

/**
 * Custom auto scan component name
		By default, Spring will lower case the first character of the component –
from ‘CustomerService’ to ‘customerService’. And you can retrieve this component with
name ‘customerService’.

To create a custom name for component, you can put custom name like this

@Service("AAA")
public class CustomerService 

Auto Components Scan Annotation Types
In Spring 2.5, there are 4 types of auto components scan annotation types

@Component – Indicates a auto scan component.
@Repository – Indicates DAO component in the persistence layer.
@Service – Indicates a Service component in the business layer.
@Controller – Indicates a controller component in the presentation layer.
So, which one to use? It’s really doesn’t matter.
Let see the source code of @Repository,@Service or @Controller.
 * */
	}
}
