
package com.auto.scanning.filter.components.services;

import org.springframework.beans.factory.annotation.Autowired;

import com.auto.scanning.filter.components.dao.CustomerDAO;

public class CustomerService {
	
	@Autowired
	CustomerDAO customerDAO;
	
	@Override
	public String toString() {
		return "CustomerService [customerDAO=" + customerDAO + "]";
	}
}
