package com.setter;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class MainClass2 {
	
	public static void main(String[] args) {
		ApplicationContext context = new ClassPathXmlApplicationContext("BeansSetterRef.xml");
		Candidate can = (Candidate) context.getBean("candidate");
		Address add = (Address) context.getBean("address");
		
/**
 * In case multiple xml files need to be loaded by the main class as in some cases,
 * the bean definitions are distributed over many xml files, then the same can be loaded
 * in the MainClass as ApplicationContext context =
 * new ClassPathXmlApplicationContext("Beans.xml","AnotherBeans.xml","OtherBeans.xml");
 * */

/**
 * In case the beanID mentioned by the ref is present in the same xml file,
 * then it can ALSO be particularly specified as <ref local=address/>
 * */
		
		System.out.println("Name: " + can.getName());
		System.out.println("Age: " + can.getAge());
		System.out.println("Address: " + can.getAddrs().getDoorNo());
		System.out.println("Street: " + add.getStreet());
		System.out.println("Area: " + add.getArea());
		
		
/**
 * A bean is created corresponding to both the classes(Candidate and Address).Make
 * changes here to add multiple property tags corresponding to the various attributes
 * present inside each of the classes. One major difference here is, the property Address
 * under Candidate does not have a direct value for itself, but is rather a separate bean.
 * Hence, instead of providing value, provide ref for this property. The value provided
 * in ref will be nothing but the beanID of the Address class.We have used setter
 * injection to set values for the properties. Hence, we use the tag property and give
 * the value to be set (which is passed to the corresponding setter methods) as the value
 * correspondingly.
 * */
	}
}
